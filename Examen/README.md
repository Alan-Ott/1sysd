# Examen B1B-CF

Créez un répertoire nommé `Examen` à la base de votre dépôt GIT,
où vous placerez vos fichiers sources et un fichier `Makefile`
(sur la base de celui fourni ici) déclenchant la compilation de
tous les fichiers sources livrés.

N'oubliez pas de publier ces fichiers (sources et `Makefile`) dans
votre dépôt GIT public.

## 1. Surface et périmètre

Source à fournir : `circle.c`

Rappel : le périmètre $`P`$ d'un cercle et la surface $`S`$ d'un disque
de rayon $`r`$ sont donnés par les formules :

$$ P = 2\times\pi\times r $$

$$ S = \pi\times r^{2} $$

Écrire deux fonctions `P` et `S` qui acceptent un rayon `r` en paramètre
et renvoient respectivement périmètre d'un cercle et surface d'un disque
de rayon `r`.

Vous pouvez utiliser une valeur approchée pour $`\pi`$ égale à 3,14159.

Tester ces fonctions dans votre programme en affichant les valeurs de
périmètre et de surface d'une valeur saisie par l'utilisateur dans le
terminal.

## 2. Inversion de MaJUScule/minUSCulE

Source à fournir : `invertcase.c`

Écrire une fonction `void invertcase(char *s)` qui accepte une chaîne de
caractère en paramètre et transforme dans cette chaîne les majuscules en
minuscule et inversement. Les caractères non alphabétiques restant inchangés.

Faites en sorte dans la fonction `main` que la fonction soit appelée
sur l'unique argument attendu par le programme et affiche la chaîne
transformée :

~~~~Bash
$ ./invertcase "tO BE oR NOT to Be!"
To be Or not TO bE!
~~~~

## 3. Factorielle

Source à fournir : `factorial.c`

Rappel : la factorielle $`n!`$ d'un nombre entier $`n`$ non nul est définie
par : 

$$ n! = 1\times 2\times 3\times ... \times n $$

Écrire une fonction `int factorial(int n)` qui renvoie la factorielle
d'un nombre entier `n`.

Comment votre fonction se comporte-t-elle si elle reçoit le paramètre
_0_ ? Est-ce correct (selon les conventions mathématiques usuelles) ?

Testez votre fonction en affichant les factorielles des nombres entiers
entre les valeurs passées en arguments lorsque le programme est exécuté :

~~~~Bash
$ ./factorial 1 9
1! = 1
2! = 2
3! = 6
4! = 24
5! = 120
6! = 720
7! = 5040
8! = 40320
9! = 362880
~~~~

## 4. Histogramme

Source à compléter : `hist.c`

Compléter le source fourni `hist.c` :

~~~~C
#include<stdio.h>
#include<stdlib.h>

void display_histogram(int tab[], int size) {
    // TODO : compléter

}

int main() {
    int values[] = { 4, 9, 8, 2, 0, 1, 10, 5, 8 };

    display_histogram(values, 9);

    exit(EXIT_SUCCESS);
}
~~~~

Compléter pour que la fonction `display_histogram` affiche
un histogramme à partir d'un tableau quelconque de valeurs
entières positives ou nulles reçu en paramètre, de cette façon :

~~~~Bash
$ ./hist
****
*********
********
**

*
**********
*****
********
~~~~

## 5. La route est longue...

Source à compléter : `paths.c`

Examinez  le source fourni dans ce répertoire `paths.c`, exécutez
le programme compilé :

~~~~C
#include<stdio.h>
#include<stdlib.h>

typedef struct step step;
struct step {
    long double x, y;
    step *next;
};

step *create_step(long double x, long double y) {
    step *newstep;

    newstep = malloc(sizeof(step));

    newstep->x = x;
    newstep->y = y;
    newstep->next = NULL;

    return newstep;
}

void show_path(step *head) {
    step *walk = head;
    int n = 0;

    while (walk) {
        printf("Step %d : (%.2Lf, %.2Lf)\n", n, walk->x, walk->y);
        n++;
        walk = walk->next;
    }
}

step *insert_step(step *head,  long double x, long double y) {
    step *walk = head, *newstep;

    newstep = create_step(x, y);

    if (head == NULL) {
        newstep->next = head;
        head = newstep;
    } else {
        while (walk->next != NULL) {
            walk = walk->next;
        }
        walk->next = newstep;
    }
    return head;
}

int main() {
    step *head = NULL;

    head = insert_step(head, 1.0, 2.0);
    head = insert_step(head, 3.0, 1.0);
    head = insert_step(head, 4.5, 3.2);
    head = insert_step(head, 2.1, 4.0);

    show_path(head);

    exit(EXIT_SUCCESS);
}
~~~~

Voici le résultat de son exécution :

~~~~Bash
$ ./paths
Step 0 : (1.00, 2.00)
Step 1 : (3.00, 1.00)
Step 2 : (4.50, 3.20)
Step 3 : (2.10, 4.00)
~~~~

Dans ce source une structure `step` est définie qui contient trois
champs : deux coordonnées du plan `x` et `y` et un pointeur vers
une autre structure du même type.

Le but est de représenter un chemin constitué de segments successifs
allant d'étape en étape sous forme d'une liste chaînée.

Rappel : dans un plan (euclidien) la distance entre deux points de
coordonnées $`(x_1, y_1)`$ et $`(x_2, y_2)`$ est :

$$ \sqrt{(x_2 - x_1)^2 + (y_2 - y_1)^2} $$

En C la racine carrée est définie dans l'en-tête `math.h` et est
fournie par la bibliothèque `libm` (le fichier `Makefile` disponible
ici fait déjà en sorte de lier les programmes avec cette bibliothèque
dans la ligne `LDFLAGS=-lm`). Consulter `man 3 sqrt` pour déterminer
quelle fonction est adaptée au type `long double`. Le calcul d'un
carré peut se faire, au choix, en multipliant un nombre par lui-même
ou bien encore avec la fonction `powl` (`man 3 powl`).

Écrire une fonction `long double path_length(step *head)` qui calcule
la longueur totale d'un chemin quelconque, testez-là sur le chemin
défini dans le programme (sa longueur est d'environ 7,428595).

