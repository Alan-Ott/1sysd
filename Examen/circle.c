#include <stdio.h>
#include <stdlib.h>

double P(double r) {
	double pi = 3.14159;
	double res = 2 * pi * r;
	return res;
}

double S(double r) {
	double pi = 3.14159;
	double res;
	res = ( pi * (r * r));
	return res;
}

int main() {

	double radius;
	printf("Enter circle radius : ");
	scanf("%lf", &radius);
	
	printf("Perimeter : %.2lf cm\n", P(radius));
	printf("Surface : %.2lf cm²\n", S(radius));

	return 0;
}
