#include<stdio.h>
#include<stdlib.h>

int factorial(int num) {
	if (num >= 1) {
		int newNum = num -1;
		int sum = num*factorial(newNum);
		printf("%d ! = %d \n", num, sum);
		return sum;
	} else {
		return 1;
	}
}

int main(int argc, char *argv[]) {
	
	int num = strtod(argv[1], NULL);

	if (num == 0) {
		printf("0! = 1");
	} else {
		factorial(num);
	}
	return 0;
}
