#include<stdio.h>
#include<stdlib.h>

int slen(char *s) {
    int l = 0;
    char *p;
    p = s;
    while (*s++) {
        l++;
    }
    return l;
}


void invertcase(char *s) {
	char tempS;
	while (*s) {
		if (*s >= 'a' && *s <= 'z') {
			tempS = (*s - 32);
		} else if (*s >= 'A' && *s <= 'Z') {
			tempS = (*s + 32);
		} else {
			tempS = 32;
		}

		*s = tempS;
		*s++;
	}

}

int main(int argc, char *argv[]) {
	char *s;
	s = argv[1];

	printf("\"%s\"\n", s);	
	invertcase(s);
	printf("\"%s\"\n", s);	
}
