#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

int slen(char *s) {
    int l = 0;
    char *p;
    p = s;
    while (*s++) {
        l++;
    }
    return l;
}

bool is_upper(char *s) {
	int upper;
	while (*s && upper) {
		if (*s >= 'a' && *s <= 'z') {
			upper = 0;
			return false;
		}
		*s++;
	}
	return true;
}

bool is_lower(char *s) {
	int lower;
	while (*s && lower) {
		if (*s >= 'A' && *s <= 'Z') {
			lower = 0;
			return false;
		}
		*s++;
	}
	return true;
}

void supper(char *s) {
	while (*s) {
		if (*s >= 'a' && *s <= 'z') {
			*s -= 32;
		}
		*s++;
	}
}

void slower(char *s) {
	while (*s) {
		if (*s >= 'A' && *s <= 'Z') {
			*s += 32;
		}
		*s++;
	}
}

char *scopy(char *s) {
	char *new, *p;
	new = malloc((slen(s) + 1)*sizeof(char));
	p = new;
	while (*s) {
		*p++ = *s++;
	}
	*p = '\0';
	return new;
}

int sequal(char *s1, char *s2) {
	while (*s1 != '\0' && *s2 != '\0' && *s1++ == *s2++) {}
	return *s1 == *s2;
}


int main() {
    char s1[] = "Hello World!";
    char s2[] = "Bonjour";
    char s3[] = "Bonjour";
    char s4[] = "bon";

    printf("Longeur de \"%s\" : %d\n", s1, slen(s1));
    
    if (sequal(s1, s2)) {
        printf("s1 et s2 sont égales\n");
    } else {
        printf("s1 et s2 sont différentes\n");
    }
     
    if (sequal(s2, s3)) {
        printf("s1 et s2 sont égales\n");
    } else {
        printf("s1 et s2 sont différentes\n");
    }
     
    if (sequal(s3, s4)) {
        printf("s1 et s2 sont égales\n");
    } else {
        printf("s1 et s2 sont différentes\n");
    }

	printf("%d \n ", is_lower(s1));

	printf("%d \n ", is_lower(s4));



	printf("\"%s\" \n ", scopy(s1));

	
	supper(s1);

	printf("\"%s\" \n ", s1);

	slower(s1);

	printf("\"%s\" \n ", s1 );
     
    return 0;
}

/*
int slen(char *s) : renvoie la longueur d’une chaîne
int is_upper(char *s) : renvoie -1 (vrai) si tous les
caractères alphabétiques de la chaîne sont majuscules, 0 sinon
int is_lower(char *s) : idem pour minuscules
void supper(char *s) : passe tous les caractères
alphabétique minuscules en majuscules, laisse les autres
inchangés
void slower(char *s) : l’inverse
char *scopy(char *s) : alloue l’espace nécessaire et y copie
la chaîne s, renvoie un pointeur vers la chaîne copiée
char *sequal(char *s1, char *s2) : renvoie -1 si les
chaînes sont égales (même contenu) et 0 sinon */

