# Calcul Simple
## Exercice

Écrire un programme qui demande deux nombres (float) à
l’utilisateur puis une opération à réaliser : +, -, *, / 

scanf(“%f”) permet de lire un float

Avec switch/case réalisez l’opération et affichez le résultat

Note: switch/case ne peut marcher avec une chaîne mais avec
un char si ! Il faudra l’utiliser avec operation[0] (si votre
variable s’apelle operation)

On peut (à vérifier) lire un char unique avec scanf (%c)
