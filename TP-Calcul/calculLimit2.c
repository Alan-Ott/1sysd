#include <stdlib.h>
#include <stdio.h>


double add(double num1, double num2) {
	return num1 + num2;
}

double subtract(double num1, double num2) {
	return num1 - num2;
}

double multiply(double num1, double num2) {
	return num1 * num2;
}

double divide(double num1, double num2) {
	return num1 / num2;
}


int main () {

	double num1, num2;
	char op;
	double res;

	printf("Enter a number between 0 and 99 : ");
	scanf("%lf ", &num1);
	printf("Enter an operator (+, -, *, /) : ");
	scanf("%c", &op);
	printf("Enter a second number between 0 and 99 : ");
	scanf("%lf ", &num2);


	switch (op) {
		case '+':
			return res = add(num1, num2);
			break;

		case '-':
			return res = subtract(num1, num2);
			break;

		case '*':
			return res = multiply(num1, num2);
			break;

		case '/': 
			return res = divide(num1, num2);
			break;

		default:
			printf("Error!\n");
			exit(EXIT_FAILURE);
	}


	printf("Result : %.2lf \n", res);
}


