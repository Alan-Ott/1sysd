# Un Makefile générique

Voici un Makefile générique que vous pourrez adapter
lors des TPs et lors de l'examen :

~~~~Makefile
.PHONY: all clean

PROGS=hello bonjour
CC=gcc
CFLAGS=
LDFLAGS=-lm

all: $(PROGS)

%: %.c
	$(CC) $(CFLAGS) -o $@ $<  $(LDFLAGS)

%.s: %.c
	$(CC) -S $< 

clean:
	rm -f $(PROGS) *.s *.o
~~~~
