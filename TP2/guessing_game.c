#include<stdlib.h>
#include<stdio.h>
#include<time.h>

int main () {
	int target = rand() % 100;
	srand(time(NULL));
	int guess;
	printf("Try to guess my number between 1 and 100!\n");
	while (guess != target) {
		printf("Enter your guess : ");
		scanf("%d", &guess);
		if (guess < target) {
			printf("\n Higher! \n");
		} else if (guess > target) {
			printf("\n Lower! \n");
		} else  {
			printf("\n Well Done! You guessed right! \n");
			break;
		}
	}
}
