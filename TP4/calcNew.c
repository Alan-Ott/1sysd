#include<stdio.h>
#include<stdlib.h>

// double au lieu de float (plus précis)

double add(double x, double y) {
    return x + y;
}

double sub(double x, double y) {
    return x - y;
}

double mul(double x, double y) {
    return x * y;
}

// divv pour ne pas avoir de conflit avec div 
// défini dans stdlib.h
double divv(double x, double y) {
    return x / y;
}

double calcEq(double a, char op, double b) {
	double res;
    	switch (op) {
		case '+':
        		res = add(a, b);
           		break;
        	case '-':
           		res = sub(a, b);
           		break;
        	case '*':
           		res = mul(a, b);
           		break;
        	case '/':
         		res = divv(a, b);
           		break;
        	default:
           		printf("Opération inconnue.\n");
           		exit(EXIT_FAILURE);
    	}
	return res;	 
}

int main(int argc, char *argv[]) {
    double a,b,res;
    char op;
	
	if (argc == 4) {
			op = argv[1][0];
    			a = strtold(argv[2], NULL);
			b = strtold(argv[3], NULL);	
    			printf("%lf %c %lf \n", a, op, b);
			
			res = calcEq(a, op, b);

			printf("Résultat : %.2lf\n", res);
	} else {
		return 1;
	}
}


