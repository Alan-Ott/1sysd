#include<stdio.h>

#define DIM 3

void print_matrix(double M[DIM][DIM]) {
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			printf("%2.2lf ", M[i][j]);
		}
		printf("\n");
	}

	printf("\n");
}

int main() {
	double A[DIM][DIM] = {{1,2,3},
			      {4,5,6},
			      {7,8,9}};

	double B[DIM][DIM] = {{2,3,4},
			      {5,6,7},
			      {8,9,10}};
	print_matrix(A);
	
	print_matrix(B);

	return 0;
}
